# Wine Quality classification exercise

## About dataset
* Paulo Cortez, University of Minho, Guimarães, Portugal, http://www3.dsi.uminho.pt/pcortez
* The two datasets are related to red and white variants of the Portuguese "Vinho Verde" wine. They both consist eleven input variables:
1. fixed acidity
2. volatile acidity
3. citric acid
4. residual sugar
5. chlorides
6. free sulfur dioxide
7. total sulfur dioxide
8. density
9. pH
10. sulphates
11. alcohol
12. output variable: quality (semantically categorical).

## Exercises
* Your main task is to create binary classification model using R or Python. Use extreme Gradient Boosting method to do that. Prepare your dataset step by step:

1. Import necessary libraries
2. Read and combine two input files (they will be considered together as one dataset)
3. Check data set structure and summary 
4. Plot quality distribution
5. Change multiple classification into binary classification
6. Perform one hot encoding if necessary
7. Split the dataset into train and test subsets with chosen split ratio
8. Prepare the structure of XGB predictive model
9. Train the XGB model
10. Perform prediction for test set
11. Create and discuss the confusion matrix
11. Present and discuss the feature importance plot